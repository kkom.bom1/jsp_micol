/**
 * noticeAjax.js
 */
// 'abcde'.slice(0,3) => 0부터 3번째 앞까지 짜른다.
Date.prototype.yyyymmdd = function() { //객체의 모든 타입은 prototype사용해 메소드 만들수 있따.
	let y = this.getFullYear();
	let m = this.getMonth() + 1; //월은 0부터 시작
	let d = this.getDate();
	let ymd = y + '-' + ('0' + m).slice(-2) + '-' + ('0' + d).slice(-2);
	return ymd;
}
let today = new Date();
console.log(today.yyyymmdd());

document.addEventListener('DOMContentLoaded', init);

function init() {

	//데이터 조회할때
	const xhtp = new XMLHttpRequest();
	xhtp.open('GET', 'noticeListAjax.do'); //서버에다가 json포맷을 호출
	xhtp.send();
	xhtp.onload = function() {
		let data = JSON.parse(xhtp.response); //json -> javascript object

		table.initData = data;
		table.showField = ['noticeId', 'noticeWriter', 'noticeTitle',
			'noticeDate', 'noticeFile', 'noticeHit'
		];
		let tb1 = table.makeTable(); //table, thead, tbody만들기
		document.getElementById('show').append(tb1);
		//thead -> 타이틀 추가 
		//table.addTitle('삭제'); 
		let th = document.createElement('th')
		th.innerText = '삭제';

		//table.addTitle('수정'); 
		let th2 = document.createElement('th')
		th2.innerText = '수정';

		document.querySelector('#show thead tr').append(th, th2);

		//tbody -> 버튼추가
		document.querySelectorAll('#show tbody tr').forEach(item => { //item=>tr
			//삭제버튼
			item.append(addDelBtn());

			//수정버튼
			item.append(addModBtn());
		})

	}

	//입력처리
	//document.querySelector('form[name=ajaxFrm]').addEventListener('submit', addNotice);
	document.querySelector('form[name=ajaxFrm]').addEventListener('submit', addMultiNotice);

}//end of 


function makeTd() {
	let td = document.createElement('td');
	let btn = document.createElement('button');
	//글삭제 
	btn.addEventListener('click', delNotice);
	btn.innerText = '삭제';
	td.append(btn);
	return td;
}

function addModBtn() {
	let td = document.createElement('td');
	let btn = document.createElement('button');
	//글수정
	btn.innerText = '수정';
	btn.addEventListener('click', modNotice);
	td.append(btn);
	return td;
}

function addMultiNotice(e){
	e.preventDefault();
	
	 //multipart/form-data처리 위한 객체
	let formData = new FormData();
	let writer = document.getElementById('writer').value;
	let title = document.getElementById('title').value;
	let subject = document.getElementById('subject').value;
	let nfile = document.getElementById('nfile');
	
	//form태그의 name속성과 이름이 달라서 맞춰주기위해 
	formData.append('noticeWriter', writer);
	formData.append('noticeTitle', title);	
	formData.append('noticeSubject', subject);
	formData.append('noticeDate', today.yyyymmdd());
	formData.append('nfile', nfile.files[0]);

	const xhtp = new XMLHttpRequest();
	xhtp.open('post', 'noticeAddAjax.do');
	xhtp.send(formData);
	xhtp.onload = function(){
		console.log(xhtp.response);
	}


}

//key:val 입력
function addNotice(e) {
	e.preventDefault(); //submit의 기능 차단(페이지 넘어가는 기능)
	let writer = document.getElementById('writer').value;
	let title = document.getElementById('title').value;
	let subject = document.getElementById('subject').value;
	const xhtp = new XMLHttpRequest();
	// xhtp.open('get', 'noticeAddAjax.do?writer=' + writer + '&title='
	// 		+ title + '&subject=' + subject + '&noticeDate='+today.yyyymmdd()); // get방식(데이터 조회할때)=> noticeAddAjax.java의 request값으로 넘겨준다
	xhtp.open('POST', 'noticeAddAjax.do'); // post방식(데이터 입력, 수정, 삭제할때) => send()의 메소드 안에 내용 넣어주고 contentType 지정해조야함
	xhtp.setRequestHeader('Content-type',
		'application/x-www-form-urlencoded') //데이터가 key value형식으로 보내겠습니다 라는 의미(contentType이 중요)
	xhtp.send('writer=' + writer + '&title=' +
		title + '&subject=' + subject + '&noticeDate=' + today.yyyymmdd());
	xhtp.onload = function(){
		let data = JSON.parse(xhtp.response);
		console.log(data); //{noticeId:?, noticeWriter:?, ....}
	//반환된 값을 활용해서 화면출력
	let tr = document.createElement('tr');
	table.showField.forEach(item => { //item => 필드이름
		console.log(item);
		let td = document.createElement('td');
		td.innerText = data[item]; //{noticeId:?, noticeTitle:?} data[noticeId]=>data[0]같은거임
		tr.append(td);
	});
	//삭제버튼
	tr.append(addDelBtn());

	//수정버튼
	tr.append(addModBtn());

	document.querySelector('#show tbody').prepend(tr);

    };
};
		

//삭제버튼 글삭제 이벤트핸들러
function delNotice() {
	//버튼이벤트 핸들러안의 this는 버튼 자신을 가리킨다.
	let id = this.parentElement.parentElement.firstChild.innerText;

	const xhtp = new XMLHttpRequest();
	xhtp.open('post', 'noticeDelAjax.do');
	xhtp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	xhtp.send('id=' + id);
	xhtp.onload = delCallback;
}

//삭제콜백함수(온로드이벤트 핸들러 => this는 xhtp)
//새로고침안하고 바로 지우기 위해 ajax쓴다
function delCallback(e) {
	console.log(e);
	let result = JSON.parse(this.response);
	if (result.retCode == 'Success') {
		console.log(result.id);
		document.querySelectorAll('#show tbody tr').forEach(item => {
			if (item.firstChild.innerText == result.id) {
				item.remove();
			}
		})
	} else if (result.retCode == 'Fail') {
		alert('처리 중 에러 발생');
	}
}

//수정버튼 글 수정 이벤트 핸들러
function modNotice() {
	let oldTr = this.parentElement.parentElement
	//cloneNode(true) => 해당 node의 children 까지 복제하려면 true, 해당 node 만 복제하려면 false
	let clone = this.parentElement.parentElement.cloneNode(true);
	console.log(clone); //이 클론에 인풋창넣어서 원래거랑 대체하기
	let writer = clone.children[1].innerText;
	let title = clone.children[2].innerText;
	let date = clone.children[3].innerText;

	//<td><input value=?></td>
	let td1 = document.createElement('td');
	let inp1 = document.createElement('input');
	inp1.value = writer;
	td1.append(inp1);
	clone.children[1].replaceWith(td1); //새로만든 td로 대체

	let td2 = document.createElement('td');
	let inp2 = document.createElement('input');
	inp2.value = title;
	td2.append(inp2);
	clone.children[2].replaceWith(td2);

	let td3 = document.createElement('td');
	let inp3 = document.createElement('input');
	inp3.setAttribute('type', 'date')
	inp3.value = date;
	td3.append(inp3);
	clone.children[3].replaceWith(td3);

	//clone의 삭제버튼 disable
	clone.children[6].firstChild.disabled = true;

	//clone의 수정 버튼-> 변경 버튼
	clone.children[7].firstChild.innerText = '변경';

	//변경버튼에 이벤트 : DB수정, 화면변경
	clone.children[7].firstChild.addEventListener('click', updateNotice);
	oldTr.replaceWith(clone);



}

//변경버튼 글 이벤트 핸들러
function updateNotice() {
	let id = this.parentElement.parentElement.firstChild.innerText;
	let writer = this.parentElement.parentElement.children[1].firstChild.value; //tr의 두번째 칸의 input태그의 value
	let title = this.parentElement.parentElement.children[2].firstChild.value;
	let date = this.parentElement.parentElement.children[3].firstChild.value;
	let param = 'id=' + id + '&writer=' + writer + '&title=' + title + '&date=' + date;

	const xhtp = new XMLHttpRequest();
	xhtp.open('post', 'noticeUpdateAjax.do');
	xhtp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	xhtp.send(param);
	xhtp.onload = updateCallback;


}

//수정콜백함수
function updateCallback() {
	console.log(this.response); //응답값=>  문자열 => 자바스크립트 객체로 파싱
	let data = JSON.parse(this.response);
	//새로운 tr생성 
	let tr = document.createElement('tr');
	table.showField.forEach(item => {
		console.log(item);
		let td = document.createElement('td');
		td.innerText = data[item];
		tr.append(td);
	});
	//삭제버튼
	tr.append(addDelBtn());
	//수정버튼
	tr.append(addModBtn());

	//새로만든 tr로 대체
	document.querySelectorAll('#show tbody tr').forEach(item => {
		if (item.firstChild.innerText == data.noticeId) {
			item.replaceWith(tr);
		}
	})

}