/**
 * noticeAjaxFetch.js fetch 활용
 */

Date.prototype.yyyymmdd = function() { //객체의 모든 타입은 prototype사용해 메소드 만들수 있따.
	let y = this.getFullYear();
	let m = this.getMonth() + 1; //월은 0부터 시작
	let d = this.getDate();
	let ymd = y + '-' + ('0' + m).slice(-2) + '-' + ('0' + d).slice(-2);
	return ymd;
}
let today = new Date();

console.log('noticeAjaxFetch start')
const showFields = {
	noticeId: "공지번호",
	noticeWriter: "작성자",
	noticeTitle: "제목",
	noticeDate: "작성일자",
	noticeHit: "조회수"
};
/*
showFields.noticeId = showFields['noticeId']
for(const prop in showFields){
	showFields.prop //==> showFields에 prop속성이 있는지 찾는다. => 없음
	//showFields.noticeId => 동적으로 값을 바꿀수없다. (for문에 쓸수없음)
	showFields[prop] // showFields['noticeId'] => 공지번호
}
*/
const addColumn = {
	col1: ['button', '삭제'], //[element 유형, 라벨]
	col2: ['button', '수정'],
	col3: ['input', 'checkbox'] //[element유형, type속성]
};
/*
addColumn.col1[1] = addColumn[col1][1]
for(const prop in addColumn){
	addColumn[prop][0] // 'button', 'input'
	addColumn[prop][1] // '삭제', '수정', 'checkbox'
}
*/

function makeTable(aryData = [], parent) {
	let tbl = document.createElement('table');
	tbl.setAttribute('border', '1')
	let thd = document.createElement('thead');
	let tbd = document.createElement('tbody');
	let tr = document.createElement('tr');

	let fields = showFields; //보여줄 항목을 지정(전체항목)
	//head영역
	for (const field in fields) {
		let th = document.createElement('th');
		th.innerText = fields[field];
		tr.append(th);
	}
	//추가항목('삭제', '수정')
	for (const col in addColumn) { //col => col1, col2
		let th = document.createElement('th');
		//addColumn[col1] =>  ['button', '삭제']
		th.innerHTML = addColumn[col][0] == 'button' ? addColumn[col][1] : '<input type = "checkbox">';
		tr.append(th)
	}

	thd.append(tr);
	tbl.append(thd);

	//body영역
	for (const data of aryData) {
		let tr = makeTr(data);
		tbd.append(tr);
	}
	tbl.append(tbd);

	parent.append(tbl); //생성된 테이블요소 매개값으로 사용된 위치에 append.

}

//tr만들기
function makeTr(data) {
	tr = document.createElement('tr');
	for (const field in showFields) {
		let td = document.createElement('td');
		td.innerText = data[field];
		tr.append(td);
	}
	//추가항목
	for (const col in addColumn) {
		let td = document.createElement('td');
		let elem = document.createElement(addColumn[col][0]);
		elem.innerText = addColumn[col][1];
		if (elem.innerText == '삭제') {
			elem.className = 'cancelbtn'; //elem.setAttribute('class', 'cancelbtn')
		}
		if (addColumn[col][0] == 'input') {
			elem.setAttribute('type', addColumn[col][1]);
		}
		td.append(elem);
		tr.append(td);


	}

	return tr;
}

//삭제 Ajax호풀
function delAjaxFnc(e) {
	e.stopPropagation()
	let id = this.parentElement.parentElement.getAttribute('id'); //notice_1, ...
	id = id.substring(7); //notice_10
	fetch('noticeDelAjax.do', {
		method: 'post', //페이지 요청 방식 지정(기본값은 get방식임), post요청(수정, 추가, 삭제)
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'  //key value형식으로 넘겨줌
			// 'application/json', 'multipart/form-data'
		},
		body: 'id=' + id

	})
		.then(result => result.json())
		.then(result => {
			if (result.retCode == 'Success') {
				document.querySelector('#notice_' + result.id).remove() //id값으로 element찾아서 제거.
			} else if (result.retCode == 'Fail') {
				alert('처리 중 오류')
			}
		})
		.catch(err => console.log(err));
}

//삭제 수정 버튼에 이벤트
function editAndDelete() {
	//삭제,수정버튼에 이벤트.
	let trs = document.querySelectorAll('#show tbody tr');
	for (const trElem of trs) {
		trElem.setAttribute('id', 'notice_' + trElem.firstChild.innerText)
		//tr클릭이벤트 ==> 수정버튼 이벤트로 변경
		trElem.addEventListener('click', showInfo)
		trElem.querySelector('td:nth-child(7)>button').addEventListener('click', showModal)
		//삭제버튼.
		trElem.querySelector('td:nth-child(6)>button').addEventListener('click', delAjaxFnc);
	}
}

//리스트 출력
fetch('noticeListAjax.do')
	.then((result) => result.json()) //stream => js의 object
	.then((data) => {
		const parentEl = document.getElementById('show');
		//table형식
		makeTable(data, parentEl);
		editAndDelete();


	})
	.catch(function(err) {
		console.log(err);
	});


document.addEventListener('DOMContentLoaded', function() {
	//form submit 이벤트(저장버튼이벤트).
	document.querySelector('form[name=ajaxFrm]').addEventListener('submit', saveNoticeFnc);
	//수정이벤트
	document.querySelector('#id01 form.modal-content.animate').addEventListener('submit', changeNoticeFnc);
})

//저장버튼 ajax호출
//submit이벤트 핸들러
function saveNoticeFnc(e) {
	e.preventDefault(); //기본기능인 form태그의submit(페이지바뀜)을 막음
	let writer = document.getElementById('writer').value;
	let title = document.getElementById('title').value;
	let subject = document.getElementById('subject').value;

	if (!writer || !title || !subject) {
		alert('값을 입력해 주세요!');
		return;
	}

	//new FromData(frm) => 매개변수로 넣어준 폼의 파일까지 읽어줘서 데이터로 만들어줌
	let frm = document.querySelector('form[name=ajaxFrm]')
	let formData = new FormData(frm); //멀티파트 요청

	fetch('noticeAddAjax.do', {
		method: 'post',
		body: formData
	})
		.then(result => result.json())
		.then(result => {
			let tr = makeTr(result);
			document.querySelector('#show tbody').prepend(tr);
			editAndDelete();
		})
		.catch(err => console.log(err))

};


//tr버튼 클릭 이벤트
function showInfo(){
	let id = this.getAttribute('id');
	id = id.substring(7);
	document.getElementById('id01').style.display = "block";
	fetch("noticeSearchAjax.do?noticeId=" + id)
		.then(result => result.json())
		.then(result => {
			console.log(result)
			document.querySelector('#id01 input[name="noticeId"]').value = result.noticeId;
			document.querySelector('#id01 input[name="writer"]').value = result.noticeWriter;
			document.querySelector('#id01 input[name="title"]').value = result.noticeTitle;
			document.querySelector('#id01 textarea[name="subject"]').value = result.noticeSubject;
			document.querySelector('#id01 input[name="noticeDate"]').value = result.noticeDate;
			document.querySelector('#id01 img').setAttribute('src', '/attech/' + result.noticeFile);
			document.querySelector('#id01 button[type="submit"]').style.visibility = 'hidden';

		})
		.catch(err => console.log(err))

}

//수정버튼클릭이벤트
function showModal(e) {
	e.stopPropagation();
	let id = this.parentElement.parentElement.getAttribute('id');
	id = id.substring(7);
	document.getElementById('id01').style.display = "block";
	fetch("noticeSearchAjax.do?noticeId=" + id)
		.then(result => result.json())
		.then(result => {
			console.log(result)
			document.querySelector('#id01 input[name="noticeId"]').value = result.noticeId;
			document.querySelector('#id01 input[name="writer"]').value = result.noticeWriter;
			document.querySelector('#id01 input[name="title"]').value = result.noticeTitle;
			document.querySelector('#id01 textarea[name="subject"]').value = result.noticeSubject;
			document.querySelector('#id01 input[name="noticeDate"]').value = result.noticeDate;
			document.querySelector('#id01 img').setAttribute('src', '/attech/' + result.noticeFile);
			document.querySelector('#id01 button[type="submit"]').style.visibility = 'visible';

		})
		.catch(err => console.log(err))

}

//변경 버튼 ajax호출
function changeNoticeFnc(e) {
	e.stopPropagation();
	e.preventDefault();
	let title = document.querySelector('#id01 input[name="title"]').value;
	let subject = document.querySelector('#id01 textarea[name="subject"]').value;
	let noticeDate = document.querySelector('#id01 input[name="noticeDate"]').value;
	let id = document.querySelector('#id01 input[name="noticeId"]').value;
	fetch('noticeUpdateAjax.do', {
		method: 'post',
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
		body: 'id=' + id + '&title=' + title + '&date=' + noticeDate + '&subject=' + subject
	})
		.then(result => result.json())
		.then(result => {
			// 모달 화면을 닫고, 서버에서 가져온 json 데이터를 활용하여, 목록에서 값을 변경
			document.querySelector('#id01').style.display = 'none';
			document.getElementById('notice_' + result.noticeId).replaceWith(makeTr(result));
			editAndDelete();
			// 기존의 Tr을 새로받아온 Tr로 대체하겠다
		})
		.catch(err => console.log(err))
}










