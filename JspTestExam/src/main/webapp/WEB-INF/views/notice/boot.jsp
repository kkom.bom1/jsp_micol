<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="boot/css/bootstrap.css">
</head>
<body>

	<div class="container mt-3" style="overflow: hidden;">

		<!-- Nav tabs -->
		<ul class="nav nav-tabs">
			<li class="nav-item"><a class="nav-link active"
				data-bs-toggle="tab" href="#home">상품상세</a></li>
			<li class="nav-item"><a class="nav-link" data-bs-toggle="tab"
				href="#menu1">큐앤에이</a></li>
			<li class="nav-item"><a class="nav-link" data-bs-toggle="tab"
				href="#menu2">리뷰</a></li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
			<div class="tab-pane container active" id="home"></div>
			<div class="tab-pane container fade" id="menu1">
				<!-- 큐앤에이 게시글 테이블 -->
				<table class="table table-sm caption-top">
					<caption>List of users</caption>
					<thead style="background-color: #F7DFDF; color: #A47171;">
						<tr>
							<th scope="col">#</th>
							<th scope="col">First</th>
							<th scope="col">Last</th>
							<th scope="col">Handle</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row">1</th>
							<td>Mark</td>
							<td>Otto</td>
							<td>@mdo</td>
						</tr>
						<tr>
							<th scope="row">2</th>
							<td>Jacob</td>
							<td>Thornton</td>
							<td>@fat</td>
						</tr>
						<tr>
							<th scope="row">3</th>
							<td colspan="2">Larry the Bird</td>
							<td>@twitter</td>
						</tr>
					</tbody>
				</table>
				
		
				<div class="container">
					<button class="btn btn-outline-secondary btn-sm" type="button"
						style="float: right;">글쓰기</button>
				</div>
				<br>

				<hr style="clear: both;">
				
				<div class="container row">
				<div class="col-3" style="width: 500px; margin: 0 auto;">
				    <form class="d-flex">
        <input class="form-control me-2 " type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success btn-outline-secondary btn-sm" type="submit">Search</button>
      </form>
      </div>
      </div>
				
				<div class="text-center">
					<ul class="pagination justify-content-center">
						<li class="page-item"><a class="page-link" href="#">Previous</a></li>
						<li class="page-item"><a class="page-link" href="#">1</a></li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item"><a class="page-link" href="#">Next</a></li>
					</ul>
				</div>
			</div>
			<div class="tab-pane container fade" id="menu2"></div>
		</div>


	</div>

	<script type="text/javascript" src="boot/js/bootstrap.js"></script>
</body>
</html>