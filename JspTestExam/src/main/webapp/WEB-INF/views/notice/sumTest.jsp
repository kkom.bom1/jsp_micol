<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<!-- include libraries(jQuery, bootstrap) -->
<link href="boot/css/bootstrap.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
<script src="boot/js/bootstrap.js"></script> 

<script src="sum/summernote-lite.js"></script>
<script src="sum/lang/summernote-ko-KR.js"></script>

<link rel="stylesheet" href="sum/summernote-lite.css">
</head>
<body>

<div class="container">

<form name="ajaxFrm" action="main.do" method="post" enctype="multipart/form-data">
<textarea name="content" id="subject" class="subject"></textarea>
<input type="submit" value="저장">

</form>
<script>
    $(document).ready(function() { 
    	$('#subject').summernote({
    	  height: 300,                 // 에디터 높이
		  minHeight: null,             // 최소 높이
		  maxHeight: null,             // 최대 높이
		  focus: true,                  // 에디터 로딩후 포커스를 맞출지 여부
		  lang: "ko-KR",					// 한글 설정
		  placeholder: '최대 2048자까지 쓸 수 있습니다'	//placeholder 설정
		   });
    	
    	$('.subject').summernote({
  		  toolbar: [
  			    // [groupName, [list of button]]
  			    ['fontname', ['fontname']],
  			    ['fontsize', ['fontsize']],
  			    ['style', ['bold', 'italic', 'underline','strikethrough', 'clear']],
  			    ['color', ['forecolor','color']],
  			    ['para', ['ul', 'ol', 'paragraph']],
  			    ['height', ['height']],
  			    ['insert',['picture','link','video']],
  			    ['view', ['fullscreen', 'help']]
  			  ],
  			fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New','맑은 고딕','궁서','굴림체','굴림','돋움체','바탕체'],
  			fontSizes: ['8','9','10','11','12','14','16','18','20','22','24','28','30','36','50','72']
  	  });
    });
  </script>
  </div>
  <script type="text/javascript">
  
  document.addEventListener('DOMContentLoaded', function() {
		//form submit 이벤트(저장버튼이벤트).
		document.querySelector('form[name=ajaxFrm]').addEventListener('submit', saveInfo);
  })
  function saveInfo(e){
	     e.preventDefault()
		console.log('서머노트')
		let subject = document.getElementById('subject').value;
		//new FromData(frm) => 매개변수로 넣어준 폼의 파일까지 읽어줘서 데이터로 만들어줌
		let frm = document.querySelector('form[name=ajaxFrm]')
		let formData = new FormData(frm); //멀티파트 요청

		fetch('noticeAddAjax.do', {
			method: 'post',
			body: formData
		})
			.then(result => result.json())
			.then(result => {
				console.log(result);
			})
			.catch(err => console.log(err))

  }
  </script>
</body>
</html>