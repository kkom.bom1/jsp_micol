package co.micol.prj.notice.map;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import co.micol.prj.notice.service.CalenderVO;
import co.micol.prj.notice.service.NoticeAttechVO;
import co.micol.prj.notice.service.NoticeVO;

public interface NoticeMapper {
	List<NoticeVO> noticeSelectList();

	NoticeVO noticeSelect(NoticeVO vo);

	int noticeInsert(NoticeVO vo);

	int noticeDelete(NoticeVO vo);

	int noticeUpdate(NoticeVO vo);

	int noticeAttechInsert(NoticeAttechVO vo);

	int noticeAttechDelete(NoticeAttechVO vo);

	void noticeHitUpdate(int id);
	
	List<NoticeVO> noticeSearchList(@Param("key") String key, @Param("val") String val);

	List<Map<String, Integer>> chartData();
	
	List<CalenderVO> calendarList();
}
