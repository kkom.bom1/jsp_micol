package co.micol.prj.notice.command;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import co.micol.prj.common.Command;
import co.micol.prj.notice.service.NoticeService;
import co.micol.prj.notice.service.NoticeVO;
import co.micol.prj.notice.serviceImpl.NoticeServiceImpl;

public class NoticeAddAjax implements Command {

	public boolean isMultiRequest(HttpServletRequest request) {
		String contentType = request.getHeader("Content-Type");
		System.out.println(contentType);
		if (contentType != null && contentType.indexOf("multipart/form-data") != -1) {
			return true;
		}
		return false;
	}

	@Override
	public String exec(HttpServletRequest request, HttpServletResponse response) {
		// 입력
		NoticeVO vo = new NoticeVO();
		if (isMultiRequest(request)) {
			// multipart 요청이면...
			// 공지사항 글등록하기 MutipartRequest를 처리해야한다.(cos.jar)
			NoticeService dao = new NoticeServiceImpl();

			String saveDir = request.getServletContext().getRealPath("attech/");
			int maxSize = 1024 * 1024 * 1024; // 최대 10m까지 업로드
			String json = null;

			try {
				// 파일 업로드시 request객체를 대체한다.
				// 멀티팔트리퀘스트객체 초기화 할때 DefaultFileRenamePolicy()=> 동일한 파일명 존재하면 뒤에 숫자 붙여줌
				// 이미 이 객채 생성 순간 파일 메모리에 올라옴
				MultipartRequest multi = new MultipartRequest(request, saveDir, maxSize, "utf-8",
						new DefaultFileRenamePolicy());

				vo.setNoticeWriter(multi.getParameter("noticeWriter"));
				vo.setNoticeDate(Date.valueOf(multi.getParameter("noticeDate")));
				vo.setNoticeTitle(multi.getParameter("noticeTitle"));
				vo.setNoticeSubject(multi.getParameter("noticeSubject"));

				// 이순간에 이미 저장
				String ofileName = multi.getOriginalFileName("nfile"); // 원본파일명
				String pfileName = multi.getFilesystemName("nfile"); // 실제저장되는 파일 이름

				if (ofileName != "") {
					vo.setNoticeFile(ofileName);
					pfileName = saveDir + pfileName; // 저장directory와 저장명
					vo.setNoticeFileDir(pfileName);

				}
				int n = dao.noticeInsert(vo);

			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {
			// multipart요청이 아닌 경우
			vo.setNoticeWriter(request.getParameter("writer"));
			vo.setNoticeTitle(request.getParameter("title"));
			vo.setNoticeSubject(request.getParameter("subject"));
			vo.setNoticeDate(Date.valueOf(request.getParameter("noticeDate")));

			NoticeService service = new NoticeServiceImpl();
			service.noticeInsert(vo);

		} // end of if(isMultiRequest)

		String json = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			json = mapper.writeValueAsString(vo);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		// {"retCode":"ok"}
		return "Ajax:" + json;

	}
}
