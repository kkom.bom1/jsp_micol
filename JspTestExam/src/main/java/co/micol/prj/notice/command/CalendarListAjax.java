package co.micol.prj.notice.command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.micol.prj.common.Command;
import co.micol.prj.notice.service.CalenderVO;
import co.micol.prj.notice.service.NoticeService;
import co.micol.prj.notice.serviceImpl.NoticeServiceImpl;

public class CalendarListAjax implements Command {

	@Override
	public String exec(HttpServletRequest request, HttpServletResponse response) {
		//캘린더 데이터 조회 기능 완성
		//NoticeService에 calendarList(): 반환타입은 List<CalenderVO>
		//json 데이터 반환.
		
		NoticeService service = new NoticeServiceImpl();
		List<CalenderVO> list = new ArrayList<CalenderVO>();
		
		list = service.calendarList();
		ObjectMapper mapper = new ObjectMapper();
		String json = null;
		try {
			json = mapper.writeValueAsString(list);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return "Ajax:"+json;
	}

}
