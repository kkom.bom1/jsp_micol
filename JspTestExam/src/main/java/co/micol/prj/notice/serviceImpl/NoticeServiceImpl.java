package co.micol.prj.notice.serviceImpl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import co.micol.prj.common.DataSource;
import co.micol.prj.notice.map.NoticeMapper;
import co.micol.prj.notice.service.CalenderVO;
import co.micol.prj.notice.service.NoticeAttechVO;
import co.micol.prj.notice.service.NoticeService;
import co.micol.prj.notice.service.NoticeVO;

public class NoticeServiceImpl implements NoticeService {
	//트루를 안쓰며 오토커밋안됨
	private SqlSession SqlSession = DataSource.getInstance().openSession(true);
	private NoticeMapper map = SqlSession.getMapper(NoticeMapper.class);
	
	@Override
	public List<NoticeVO> noticeSelectList() {
		return map.noticeSelectList();
	}

	@Override
	public NoticeVO noticeSelect(NoticeVO vo) {
		noticeHitUpdate(vo.getNoticeId()); //조회수 증가
		return map.noticeSelect(vo);
	}

	@Override
	public int noticeInsert(NoticeVO vo) {
		return map.noticeInsert(vo);
	}

	@Override
	public int noticeDelete(NoticeVO vo) {
		return map.noticeDelete(vo);
	}

	@Override
	public int noticeUpdate(NoticeVO vo) {
		return map.noticeUpdate(vo);
	}

	@Override
	public int noticeAttechInsert(NoticeAttechVO vo) {
		return map.noticeAttechInsert(vo);
	}

	@Override
	public int noticeAttechDelete(NoticeAttechVO vo) {
		return map.noticeAttechDelete(vo);
	}

	@Override
	public List<NoticeVO> noticeSearchList(String key, String val) {
		return map.noticeSearchList(key, val);
	}

	@Override
	public void noticeHitUpdate(int id) {
		map.noticeHitUpdate(id);
		
	}

	@Override
	public List<Map<String, Integer>> chartData() {
		// TODO Auto-generated method stub
		return map.chartData();
	}

	@Override
	public List<CalenderVO> calendarList() {
		// TODO Auto-generated method stub
		return map.calendarList();
	}
	

}
