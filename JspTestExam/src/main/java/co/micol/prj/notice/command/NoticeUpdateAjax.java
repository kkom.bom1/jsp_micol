package co.micol.prj.notice.command;

import java.sql.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.micol.prj.common.Command;
import co.micol.prj.notice.service.NoticeService;
import co.micol.prj.notice.service.NoticeVO;
import co.micol.prj.notice.serviceImpl.NoticeServiceImpl;

public class NoticeUpdateAjax implements Command {

	@Override
	public String exec(HttpServletRequest request, HttpServletResponse response) {
		// 넘어간 param을 받아와서 noticeUpdate
		// 파라미터 다 읽어드리고 업데이트 변수 => json반환
		NoticeVO vo = new NoticeVO();
		vo.setNoticeId(Integer.parseInt(request.getParameter("id")));
		vo.setNoticeWriter(request.getParameter("writer"));
		vo.setNoticeTitle(request.getParameter("title"));
		vo.setNoticeDate(Date.valueOf(request.getParameter("date")));
		vo.setNoticeSubject(request.getParameter("subject"));

		NoticeService service = new NoticeServiceImpl();
		int UpCnt = service.noticeUpdate(vo);
		String json = null;
		ObjectMapper mapper = new ObjectMapper();

		if (UpCnt > 0) {
			// 수정처리 후 ajax 응답에 id의 다른 정보까지 넘겨줌(조회수와 파일)
			vo = service.noticeSelect(vo);
			//json포맷으로 바꿔줌
			try {
				json = mapper.writeValueAsString(vo);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "Ajax:" + json;
		}
		return "Ajax:" + "{\"retCode\": \"Fail\"}";

	}

}
