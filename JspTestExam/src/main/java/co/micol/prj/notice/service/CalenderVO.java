package co.micol.prj.notice.service;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CalenderVO {
	private String title;
	private String startDate;
	private String endDate;
}
