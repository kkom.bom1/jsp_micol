package co.micol.prj.member.service;

import lombok.Getter;
import lombok.Setter;  

//DTO => DAO에 값을 실어서 줌
//@Data //-> 어노테이션 쓰면 자동으로 롬복이 개터새터 크게 만들어짐/
// -> 투스트링 만듬(java에서는 투스트링도 필요하니까 걍 @data쓰는게 편함)
//jsp에서는 투스트링 필요없이 개별만 필요하니까 따로 만들어줌
@Setter 
@Getter
public class MemberVO {
	private String memberId;
	private String memberName;
	private String memberPassword;
	private int memberAge;
	private String memberAddress; 
	private String memberTel;
	private String memberAuthor;
}
